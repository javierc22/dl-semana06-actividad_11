# Construir un programa que permita ingresar un número por teclado e imprimir
# la tabla de multiplicar del número ingresado. Debe repetir la operación hasta
# que se ingrese un 0 (cero).
# Ingrese un número (0 para salir): _

numero = 1

while numero !=0
  puts 'Ingrese un número para saber su tabla de multiplicar (0 para salir)...'
  numero = gets.chomp.to_i

  if numero !=0 
    10.times { |i| puts "#{numero} * #{i + 1} = #{numero * (i + 1)}" }
  end
end

puts 'Has salido...adiós.'