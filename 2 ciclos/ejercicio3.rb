# Mostrar todos los divisores del número 990 con:
# while, for, times.

puts 'Con While:'
a = 1
while a <= 990
  print "#{a}, " if (990 % a).zero?
  a += 1
end

puts "\nCon For:"
for b in 1..990 do
  print "#{b}, " if (990 % b).zero?
end

puts "\nCon Times:"
990.times { |c| print "#{c + 1}, " if (990 % (c + 1)).zero? }